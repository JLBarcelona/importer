<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class IntegrationGogetssl extends Model
{
    protected $table = 'integration_gogetssl';
    protected $guarded = [];


	public function saveGroup()
    {

        $search = Order_Group::where('name','gogetssl')->first();
		
		$group = new Order_Group();
		if($search) {
			$group = $search;
			return $group->id;
		}

		$group->user_id = Auth::User()->id;
        $group->name = 'gogetssl';
        $group->description = '';
        $group->url = 'gogetssl';
        $group->type = 1;
		
		if($group->save()){
			return $group->id;
		}
        return null;
    }
	
	public function savePackage($data, $groupId, $packageId = 'new')
    {
		
        try {
            $group = Order_Group::findOrFail($groupId);
		} catch (\Exception $e) {
            return false;
        }

        $errors = [];

        if ($packageId !== 'new') {
            $package = Package::findOrFail($packageId);
        } else {
            $package = new Package();
        }
		
		$search = Package::where('name',$data['product']);
			if($search->count() > 0) {
				$package = $search->first();
				$package->group_id = $groupId;
		        $package->name = $data['product'];
		        $package->description = "";
		        $package->tax = 1;
		        $package->prorate = 0;
		        $package->url = "";
		        $package->trial = 0;
		        $package->is_featured = 0;
		        $package->exclude_from_api = 0;
		        $package->is_outofstock = 0;
				$package->integration = 'gogetssl';
				$package->package_details = json_encode($data);
			}else{
				// $package->id = $data['id'];
		       	// $package->id = $data['id'];
		        $package->group_id = $groupId;
		        $package->name = $data['product'];
		        $package->description = "";
		        $package->tax = 1;
		        $package->prorate = 0;
		        $package->url = "";
		        $package->trial = 0;
		        $package->is_featured = 0;
		        $package->exclude_from_api = 0;
		        $package->is_outofstock = 0;
				$package->integration = 'gogetssl';
				$package->package_details = json_encode($data);
			}

        if($package->save()) {
			if($this->savePackageCycle($package)){				
				$this->saveOrderPackageSettings($package, $data);
				return true;
			}
		}
		
		return false;
    }
	
    public function saveOrderPackageSettings($package, $data){
    	$integration = 'gogetssl';
    	$datas = ['package','product', 'id', 'max_period', 'brand', 'product_type'];

    	foreach ($datas as $details) {
    		$packages = [];

    		$data_name = ($details == 'package')? 'gogetssl.package' : 'gogetssl.'.$details;
    		$data_entry = ($details == 'package')? $data['id'] : $data[$details];

    		if ($package->settings()->where('name', 'gogetssl.'.$details)->count() > 0) {
    			$packages = $package->settings()->where('name', $data_name)->first();
    			$packages->value = $data_entry;
    			$packages->save();
			}else{
				$package->settings()->create([
			        'name' => $data_name,
			        'value' => $data_entry
			    ]);
			}

    	}

    }

	public function savePackageCycle($package)
	{
		
		$cycles = Package_Cycle::where('package_id', $package->id);
		if ($cycles->count() > 0) {
			$cycle = $cycles->first();
			$cycle->package_id = $package->id;
			$cycle->price =  1.00;
			$cycle->fee = 0.00;
			$cycle->cycle =  1;
			
			if($cycle->save()){

				return  true;
				
			}
		}else{
			$cycle = new Package_Cycle();
			$cycle->package_id = $package->id;
			$cycle->price =  1.00;
			$cycle->fee = 0.00;
			$cycle->cycle =  1;
			
			if($cycle->save()){

				return  true;
				
			}
		}

		
		
		return false;
		
		
	}
}
