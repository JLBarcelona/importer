<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrationPlesk extends Model
{
    protected $table = 'integration_plesks';
    protected $guarded = [];
}
