<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VPN_Branding extends Model
{
    protected $connection = 'site';
    protected $table = 'vpn_branding';
    protected $fillable = ['business_name',	'support_email','filename_logo','path_logo','filename_homepage1','path_homepage1','filename_homepage2','path_homepage2','filename_homepage3','path_homepage3'];
    protected $hidden = ['id', 'created_at', 'updated_at'];

}