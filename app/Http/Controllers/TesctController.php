<?php

namespace App\Http\Controllers;

use Config;
use App\Address;
// use App\Counties;
use App\Countries;
// use App\Currency;
// use App\Invoice;
// use App\Order;
use App\Order_Group;
// use App\Package;
// use App\Package_Cycle;
// use App\Repositories\ActivationRepository;
// use App\SupportTicket;
// use App\SupportTicketMessage;
// use App\Transaction;
// use App\Transactions;
use App\User;
use App\User_Contact;
// use App\User_Link;
// use App\User_Setting;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Laracsv\Export;
use Mail;
use App\Mail\GeneralEmail;
use Settings;
use Validator;

class TestController extends Controller
{


    public function test(){
        return 1;
    }

	public function execute(){
        // $this->pricessDBBySqlFile('whmcs.sql');
        return $this->processWHMSQL();
	}

	public function processWHMSQL(array $data = []){
        session()->put('_status_log', 'Import data initiate');
        DB::beginTransaction();
        $auth_user = 1;
        $data['tblproductgroups'] = $this->getDataByTable('tblproductgroups');
        $data['tblproducts'] = $this->getDataByTable('tblproducts');
        $data['tblpricing'] = $this->getDataByTable('tblpricing');
        $data['tblorders'] = $this->getDataByTable('tblorders');
        $data['tblhosting'] = $this->getDataByTable('tblhosting');
        $data['tblinvoices'] = $this->getDataByTable('tblinvoices');
        $data['tblinvoiceitems'] = $this->getDataByTable('tblinvoiceitems');
        $data['tblcurrencies'] = $this->getDataByTable('tblcurrencies');
        $data['tblclients'] = $this->getDataByTable('tblclients');
        $data['tbltickets'] = $this->getDataByTable('tbltickets');
        $data['tblticketreplies'] = $this->getDataByTable('tblticketreplies');

        $data = array_filter($data);

        // if (Order_Group::count() == 0) {
        //     DB::statement("ALTER TABLE order_groups AUTO_INCREMENT = 1");
        // }

        // if (Package::count() == 0) {
        //     DB::statement("ALTER TABLE order_group_packages AUTO_INCREMENT = 1");
        // }

        // if (Package_Cycle::count() == 0) {
        //     DB::statement("ALTER TABLE order_group_package_cycles AUTO_INCREMENT = 1");
        // }

        // first

        if (isset($data['tblclients'])) {
           $clients = $data['tblclients']->select(DB::raw('concat(firstname," ",lastname) as name'),'email','password','datecreated', 'country', 'companyname', 'phonenumber', 'postcode', 'address1', 'address2', 'city', 'updated_at', 'created_at')->get();
           $client_insert = [];
           $addresses = [];
           $emails = [];
           $user_profiles = [];
           foreach ($clients as $client) {
            // phase 1
                $emails[] = $client->email; 
                $client_insert[] = [
                    'email' => $client->email,
                    'name' => $client->name,
                    'account_type' => 2,
                    'authEnabled' => 0,
                    'authSecret' => 0,
                    'stripeId' => 0,
                    'vat_number' => 0,
                    'fraudlabs_status' => '',
                    'fraudlabs_json' => '',
                    'sandbox_api_key' => '',
                    'live_api_key' => '',
                    'last_login' => '2022-11-18 10:11:20',
                    'password' => !empty($client->password) ? $client->password : '',
                    'created_at' => !empty($client->datecreated) ? date('Y-m-d H:i:s', strtotime($client->datecreated)) : now(),
                    'username' => $client->email,
                ];

                $country = !empty($client->country) ? Countries::select('id')->where('iso2', 'like', '%'.strtoupper($client->country).'%')->first() : null;
 
                $address_check = Address::where('email', $client->email);
                if ($address_check->count() > 0) {
                    $addresses_update = [
                        'contact_name' => $client->name,
                        'business_name' => $client->companyname,
                        'phone' => $client->phonenumber,
                        'fax' => '',
                        'website' => '',
                        'address_1' => $client->address1,
                        'address_2' => $client->address2,
                        'address_3' => '',
                        'address_4' => '',
                        'city' => $client->city,
                        'postal_code' => $client->postcode,
                        'country_id' => $country ? $country->id : null,
                        'created_at' => $client->created_at,
                        'updated_at' => $client->updated_at
                    ];

                    $address_check->update($addresses_update);

                }else{
                    $addresses[] = [
                        'email' => $client->email,
                        'contact_name' => $client->name,
                        'business_name' => $client->companyname,
                        'phone' => $client->phonenumber,
                        'fax' => '',
                        'website' => '',
                        'address_1' => $client->address1,
                        'address_2' => $client->address2,
                        'address_3' => '',
                        'address_4' => '',
                        'city' => $client->city,
                        'postal_code' => $client->postcode,
                        'country_id' => $country ? $country->id : null,
                        'created_at' => $client->created_at,
                        'updated_at' => $client->updated_at
                    ];
                }
           }
            
           try {
                $insert_user = User::upsert($client_insert, ['email']);
                $insert_address = Address::insert($addresses);
               DB::commit();
           } catch (Exception $e) {
               DB::rollBack();
               return $e->getMessage();
               
           }

           $user_contact = [];
           foreach ($emails as $mail) {
                $user_email_check = User::select('id')->where('email', $mail)->first();
                $address_check = Address::select('id')->where('email', $mail)->first();
                $user_and_address[] = ['user_id' => $user_email_check->id, 'address_id' => $address_check->id];

                $user_contact [] = [
                            'user_id' => $user_email_check->id,
                            'address_id' => $address_check->id,
                            'type' => '1',
                             ];
           }

            try {
                User_Contact::insert($user_contact);
               DB::commit();
           } catch (Exception $e) {
               DB::rollBack();
               return $e->getMessage();
               
           }
            // phase 1
            if (isset($data['tblproductgroups'])) {
                $product_groups_ids = [];
                $tblproductgroups = $data['tblproductgroups']->select('id','headline','tagline','hidden','name','created_at','updated_at')->get(); 
                foreach ($tblproductgroups as $productGroup) {
                    $headline = $productGroup->headline ?? 'null';
                    $tagline = $productGroup->tagline ?? 'null';
                    $hidden = (int) !empty($productGroup->hidden) && $productGroup->hidden === 0 ? '1' : '0';

                    $description = "{$headline} {$tagline}";


                 $check_group = Order_Group::where([
                                'user_id' => $auth_user,
                                'name' => $productGroup->name ?? null,
                                'description' => $description,
                                'url' => Str::slug($productGroup->name),
                                'visible' => $hidden,
                            ]);

                    if ($check_group->count() > 0) {
                       $has_group = $check_group->first();
                       $product_groups_ids[] = $has_group->id;
                    }else{
                         $product_groups_insert =  [
                            'user_id' => $auth_user,
                            'name' => $productGroup->name,
                            'description' => $description,
                            'url' => Str::slug($productGroup->name),
                            'visible' => $hidden,
                            'created_at' => $productGroup->created_at,
                            'updated_at' => $productGroup->updated_at,
                            'type' => 2
                        ];

                        $product_groups_id = Order_Group::create($product_groups_insert);
                        $product_groups_ids[] = $product_groups_id->id;
                    }
                }
            }

            foreach ($product_groups_ids as $pgroupId) {
               if (isset($data['tblproducts']) && isset($data['tblpricing'])) {
                  $products = $data['tblproducts']->where('gid', $pgroupId)->get();
                  foreach ($products as $product) {
                      
                  }

               }
            }

            // return $product_groups_ids;
            // current
           // return response()->json(['insert' => 'success']);
        }

        die;
        

        if (isset($data['tbltickets'])) {
            foreach ($data['tbltickets'] as $ticket) {

                // try {

                $tcket = new SupportTicket();

                $tcket->user_id = Auth::user()->id;
                $tcket->subject = !empty($ticket['title']) ? $ticket['title'] : '';
                $tcket->status = !empty($ticket['status']) ? strtolower($ticket['status']) : '';
                $tcket->priority = strtolower($ticket['urgency']);
                $tcket->last_action = !empty($ticket['lastreply']) ? Carbon::parse($ticket['lastreply']) : Carbon::now();
                $tcket->created_at = !empty($ticket['date']) ? $ticket['date'] : Carbon::now();

                try {
                    $tcket->save();
                } catch (\Exception $exception) {
                    DB::rollBack();
                    Log::warning($exception->getMessage());

                    return redirect()->back()->withErrors($exception->getMessage());
                }

                if (isset($data['tblticketreplies'])) {
                    $replies = collect($data['tblticketreplies'])->where('tid', $ticket['id']);

                    // Log::warning([
                    //     'table' => 'tblticketreplies',
                    //     'title' => 'Data list tblticketreplies by ticket tid',
                    //     'data' => [
                    //         'ticker_id' => $ticket['id'],
                    //         'ticker_tid' => $ticket['tid'],
                    //         'count_replies' => $replies->count(),
                    //         'total_countRecords' => count($data['tblticketreplies']),
                    //     ],
                    // ]);

                    $replies->each(function ($reply) use ($tcket) {
                        $repl = new SupportTicketMessage();

                        $repl->support_ticket_id = $tcket->id;
                        $repl->replay_by = !empty($reply['userid']) ? $reply['userid'] : '';
                        $repl->message = !empty($reply['message']) ? $reply['message'] : '';
                        $repl->created_at = !empty($reply['date']) ? $reply['date'] : Carbon::now();

                        try {
                            $repl->save();
                        } catch (\Exception $exception) {
                            DB::rollBack();
                            Log::warning($exception->getmessage());

                            return redirect()->back()->withErrors($exception->getMessage());
                        }
                    });
                }

                // } catch (\Exception $exception) {
                //     Log::warning([
                //         'table' => 'tbltickets',
                //         'message' => $exception->getMessage(),
                //         'ticket' => $ticket,
                //     ]);
                // }
            }
        }

        $currency = 0;

        if (isset($data['tblcurrencies'])) {
            $lastCurrency = User_Setting::select('name')
                ->where('name', 'like', '%invoices.currency%')
                ->latest('name')
                ->first();

            if ($lastCurrency) {
                $lastName = (substr($lastCurrency->name, -1) + 1);
            } else {
                $lastName = 1;
            }

            foreach ($data['tblcurrencies'] as $currency) {
                $currency = Currency::where('short_name', 'like', '%'.strtoupper($currency['code']).'%')->first();

                $userSetting = User_Setting::where('user_id', Auth::user()->id)
                    ->where('name', 'invoice.currency.'.$lastName)
                    ->first()
                ;

                if (!$userSetting) {
                    $userSetting = new User_Setting();

                    $userSetting->user_id = Auth::user()->id;
                    $userSetting->name = 'invoice.currency.'.$lastName;
                }

                if ($currency) {
                    $userSetting->value = $currency->id;
                }

                try {
                    $userSetting->save();
                } catch (\Exception $exception) {
                    DB::rollBack();
                    Log::warning($exception->getMessage());

                    return redirect()->back()->withErrors($exception->getMessage());
                }
            }
        }

        // $nameDB = env('DB_DATABASE_WHMCS', 'whmcs');

        // DB::statement("DROP DATABASE {$nameDB}");

        /**
         * Revoke current user database from improt db
         */
        // $this->revokeUser();
        // $contents = [
        //             'greetings' => 'Hello,',
        //             'message' => 'Your SQL file has imported successfully!'
        //            ];

        // Mail::to('jordan@baseserv.com')->send(new GeneralEmail('no-reply@onlinebillingform.com', 'no-reply@onlinebillingform.com', 'WHMCS', $contents, 'Notifications.notifications'));
        // JL mail
        // return redirect()->back()->with('message', 'Import completed successfully!');
    }


    public function getDataByTable($table)
    {
         return DB::connection('mysql')
            ->table($table);

        // return array_map(function($item) {
        //     return (array) $item; 
        // }, $result->toArray());
    }

     public function revokeUser(){
        /**
         * Database name
         *
         * @var string
         */
        $nameDB = env('DB_DATABASE_WHMCS', 'whmcs');

        /**
         * Get host of current site connection
         *
         * @var string
         */
        $siteHost = Config::get('database.connections.site.host');

        /**
         * Get username of current site connection
         *
         * @var string
         */
        $siteUsername = Config::get('database.connections.site.username');

        /**
         * Add permissions for $nameDB database
         * for current username
         */
        DB::connection('main')->statement(
            "REVOKE CREATE, ALTER ROUTINE, ALTER, SHOW VIEW, SELECT, EXECUTE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, GRANT OPTION, LOCK TABLES  ON {$nameDB}.* FROM {$siteUsername}@{$siteHost};"
        );
        DB::connection('main')->statement("FLUSH PRIVILEGES;");
    }

    public function transformSQLintoArray($file){
        ini_set('memory_limit', -1);
        $opData = '';
        $queries = [];
        $result = file(storage_path($file));

        if (! $result) {
            return redirect()->back()->withErrors('File not found.');
        }

        foreach ($result as $line) {
            if (substr($line, 0, 2) == '--' || $line == '') {
                continue;
            }

            $opData .= $line;

            if (substr(trim($line), -1, 1)) {
                $queries[] = $opData;
                $opData = '';
            }
        }

        foreach ($queries as $key => $query) {
            $queries[$key] = str_replace(PHP_EOL, '', $query);
        }

        return $queries;
    }

    public function pricessDBBySqlFile($file)
    {

        ini_set('memory_limit', -1);
        /**
         * Database name
         *
         * @var string
         */
        $nameDB = env('DB_DATABASE_WHMCS', 'whmcs');

        try {
            DB::statement("DROP DATABASE {$nameDB}");
        } catch (\Exception $e) {}

        $host = env('DB_HOST_WHMCS', 'localhost');
        $username = env('DB_USERNAME', 'forge');
        $password = env('DB_PASSWORD', '');

        /**
         * Get host of current site connection
         *
         * @var string
         */
        $siteHost = Config::get('database.connections.site.host');

        /**
         * Get username of current site connection
         *
         * @var string
         */
        $siteUsername = Config::get('database.connections.site.username');

        /**
         * Add permissions for $nameDB database
         * for current username
         */
        DB::connection('main')->statement(
            "GRANT EXECUTE, SELECT, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON {$nameDB}.* TO {$siteUsername}@{$siteHost} WITH GRANT OPTION;

            "
        );
        DB::connection('main')->statement("FLUSH PRIVILEGES;");

        /**
         * Create database for import
         */
        DB::statement("CREATE DATABASE {$nameDB} CHARACTER SET utf8 COLLATE utf8_general_ci");

        \Config::set("database.connections.whmcs", [
            'driver' => 'mysql',
            'host' =>  $host,
            'database' => $nameDB,    
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'prefix' => '',
            'collation' => 'utf8_unicode_ci',
            'strict'    => false,
            // 'options' => [
            //     \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            //     \PDO::ATTR_EMULATE_PREPARES => true,
            // ],
        ]);

        /**
         * Get import SQL dump file
         *
         * @vra string
         */
        $sql = file_get_contents(storage_path($file));

        /**
         * Path to import SQL dump file
         *
         * @var string
         */
        $filePath = storage_path($file);

        /**
         * mysql cmd command
         *
         * @var string
         */
        $cmd = "mysql --host {$host} --user {$username} --password=\"{$password}\" {$nameDB} < {$filePath}";

        exec($cmd, $output, $retval);

        // DB::connection('whmcs')->unprepared($sql);

        // $pdo = DB::connection('whmcs')->getPdo();
        // $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        // $pdo->exec($sql);

        // DB::connection('whmcs')->getPdo()->exec($sql);
    }

}
