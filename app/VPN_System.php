<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VPN_System extends Model
{
    protected $connection = 'site';
    protected $table = 'vpn_system';
    protected $fillable = ['server_name' ,'ip_address' ,'ssh_port' ,'ssh_username' ,'ssh_password' ,'max_connections','vpn_type','flag_countries'];
    protected $hidden = ['created_at', 'updated_at'];

}
