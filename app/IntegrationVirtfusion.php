<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrationVirtfusion extends Model
{
    protected $table = 'integration_virtfusion';
    protected $guarded = [];
}
